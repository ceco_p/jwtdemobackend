package com.privatephone.adminpanel.config;

import com.privatephone.adminpanel.security.JwtAuthenticationProvider;
import com.privatephone.adminpanel.security.JwtSettings;
import com.privatephone.adminpanel.security.SkipUrlRequestMatcher;
import com.privatephone.adminpanel.security.filters.JwtTokenAuthenticationProcessingFilter;
import com.privatephone.adminpanel.security.services.DbUserDetailsService;
import com.privatephone.adminpanel.security.services.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;

@EnableWebSecurity
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {

    public static final String AUTHENTICATE_URL = "/api/auth/authenticate";
    public static final String REFRESH_TOKEN_URL = "/api/auth/refresh-token";
    public static final String REVOKE_TOKEN_URL = "/api/auth/revoke-token";
    public static final String API_ROOT_URL = "/api/**";

    @Autowired
    private JwtService jwtService;
    @Autowired
    private DbUserDetailsService userDetailsService;
    @Autowired
    private AuthenticationFailureHandler failureHandler;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
        auth.authenticationProvider(new JwtAuthenticationProvider(jwtService));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        String[] permitAllEndpoints = {AUTHENTICATE_URL,
                REFRESH_TOKEN_URL,
                REVOKE_TOKEN_URL};

        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests().antMatchers(permitAllEndpoints).permitAll()
                .anyRequest().authenticated();

        http.addFilterBefore(createJwtTokenAuthenticationProcessingFilter(failureHandler, permitAllEndpoints),
                UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private Filter createJwtTokenAuthenticationProcessingFilter(AuthenticationFailureHandler failureHandler, String[] permitAllEndpointList) {
        SkipUrlRequestMatcher matcher = new SkipUrlRequestMatcher(permitAllEndpointList, API_ROOT_URL);
        JwtTokenAuthenticationProcessingFilter filter = new JwtTokenAuthenticationProcessingFilter(failureHandler, matcher);
        filter.setAuthenticationManager(authenticationManager);
        return filter;
    }

}
