package com.privatephone.adminpanel.security;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SkipUrlRequestMatcher implements RequestMatcher {

    private RequestMatcher matcher;
    private AntPathRequestMatcher requestMatcher;

    public SkipUrlRequestMatcher(String[] skipPaths, String requestPath) {
        List<RequestMatcher> matchers = Arrays.stream(skipPaths).map(e -> new AntPathRequestMatcher(e)).collect(Collectors.toList());
        this.matcher = new OrRequestMatcher(matchers);
        requestMatcher =  new AntPathRequestMatcher(requestPath);
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        if (matcher.matches(request)) {
            return false;
        }
        return requestMatcher.matches(request);
    }
}
