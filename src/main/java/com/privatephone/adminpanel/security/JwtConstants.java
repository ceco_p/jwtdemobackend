package com.privatephone.adminpanel.security;

public class JwtConstants {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String AUTHORIZATION_HEADER_PREFIX = "Bearer ";

}
