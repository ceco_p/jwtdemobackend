package com.privatephone.adminpanel.security.auth;

import com.privatephone.adminpanel.security.models.UserContext;
import org.springframework.security.authentication.AbstractAuthenticationToken;

public class JwtAuthenticationToken extends AbstractAuthenticationToken {

    private String accessToken;
    private UserContext userContext;

    public JwtAuthenticationToken(String accessToken) {
        super(null);
        super.setAuthenticated(false);
        this.accessToken = accessToken;
    }

    public JwtAuthenticationToken(String accessToken, UserContext userContext) {
        super(userContext.getGrantedAuthority());
        this.setAuthenticated(true);
        this.accessToken = accessToken;
        this.userContext = userContext;
    }

    @Override
    public Object getCredentials() {
        return accessToken;
    }

    @Override
    public Object getPrincipal() {
        return userContext;
    }
}
