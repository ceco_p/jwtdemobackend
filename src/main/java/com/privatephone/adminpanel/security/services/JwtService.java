package com.privatephone.adminpanel.security.services;

import com.privatephone.adminpanel.mappers.AdminUsersMapper;
import com.privatephone.adminpanel.mappers.JwtTokenMapper;
import com.privatephone.adminpanel.models.AdminUser;
import com.privatephone.adminpanel.models.JwtRefreshTokenInfo;
import com.privatephone.adminpanel.security.JwtSettings;
import com.privatephone.adminpanel.security.exceptions.InvalidJwtTokenException;
import com.privatephone.adminpanel.security.exceptions.JwtExpiredTokenException;
import com.privatephone.adminpanel.security.models.JwtToken;
import com.privatephone.adminpanel.security.models.UserContext;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class JwtService {
    private static Logger logger = LoggerFactory.getLogger(JwtService.class);

    private static final String ROLE_CLAIM_KEY = "roles";
    private static final String REFRESH_TOKEN_ROLE = "REFRESH_ROLE";
    private JwtSettings settings;

    @Autowired
    private JwtTokenMapper jwtTokenMapper;
    @Autowired
    private AdminUsersMapper adminUsersMapper;

    @Autowired
    public JwtService(JwtSettings settings) {
        this.settings = settings;
    }

    public JwtToken generateAccessToken(UserContext userInfo) {
        List<String> roles = userInfo.getGrantedAuthority().stream()
                .map(grantedAuthority -> grantedAuthority.getAuthority())
                .collect(Collectors.toList());
        Claims claims = Jwts.claims();
        claims.put(ROLE_CLAIM_KEY, roles);

        JwtToken token = new JwtToken();
        token.setExpDate(getExpireDate(settings.getTokenExpirationTime()));
        token.setToken(createToken(claims,
                userInfo.getUsername(),
                null,
                token.getExpDate()));
        return token;
    }

    public UserContext parseAccessToken(String encodedToken) {
        try {
            Claims body = Jwts.parser().setSigningKey(settings.getSecret()).parseClaimsJws(encodedToken).getBody();
            List<String> roles = body.get(ROLE_CLAIM_KEY, List.class);
            List<GrantedAuthority> grantedAuthority = roles.stream()
                    .map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());

            UserContext userInfo = new UserContext();
            userInfo.setUsername(body.getSubject());
            userInfo.setGrantedAuthority(grantedAuthority);

            return userInfo;
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException ex) {
            logger.error("Invalid JWT Token", ex);
            throw new BadCredentialsException("Invalid JWT token: ", ex);
        } catch (ExpiredJwtException expiredEx) {
            logger.info("JWT Token is expired", expiredEx);
            throw new JwtExpiredTokenException("JWT Token expired", expiredEx);
        }
    }

    public JwtToken parseRefreshToken(String encodedToken) throws InvalidJwtTokenException {
        if (encodedToken == null || encodedToken.isEmpty()) {
            throw new InvalidJwtTokenException("Missing Authorization header.");
        }

        Claims body = Jwts.parser().setSigningKey(settings.getSecret()).parseClaimsJws(encodedToken).getBody();

        JwtToken jwtToken = new JwtToken();
        jwtToken.setUuid(body.getId());
        jwtToken.setExpDate(body.getExpiration());
        jwtToken.setUserName(body.getSubject());

        return jwtToken;
    }

    public void validateRefreshToken(JwtToken refreshToken) throws InvalidJwtTokenException {
        int tokenCount = jwtTokenMapper.countByUuid(refreshToken.getUuid());
        if (tokenCount == 0) {
            throw new InvalidJwtTokenException("The token is invalid");
        }

        AdminUser adminUser = adminUsersMapper.findAdminUserByName(refreshToken.getUserName());
        if (adminUser == null) {
            throw new UsernameNotFoundException("The user do not exists");
        }
    }

    public void saveRefreshToken(JwtToken refreshToken, String userName) {
        JwtRefreshTokenInfo tokenInfo = jwtTokenMapper.findByUserName(userName);

        if (tokenInfo == null) {
            tokenInfo = new JwtRefreshTokenInfo();
            tokenInfo.setExpDate(refreshToken.getExpDate());
            tokenInfo.setUserName(userName);
            tokenInfo.setUuid(refreshToken.getUuid());
            jwtTokenMapper.insetJwtToken(tokenInfo);
        } else {
            tokenInfo.setExpDate(refreshToken.getExpDate());
            tokenInfo.setUuid(refreshToken.getUuid());
            jwtTokenMapper.updateJwtToken(tokenInfo);
        }
    }

    public void revokeToken(String token) {
        try {
            JwtToken refreshToken = parseRefreshToken(token);
            jwtTokenMapper.deleteJwtToken(refreshToken.getUuid());
        } catch (InvalidJwtTokenException e) {
            logger.warn("Revoke token: token is invalid.");
        }
    }

    public JwtToken generateRefreshToken(UserContext userInfo) {
        Claims claims = Jwts.claims();
        claims.put(ROLE_CLAIM_KEY, new String[]{REFRESH_TOKEN_ROLE});
        JwtToken token = new JwtToken();
        token.setExpDate(getExpireDate(settings.getRefreshTokenExpirationTime()));
        token.setUuid(UUID.randomUUID().toString());
        token.setToken(createToken(claims,
                userInfo.getUsername(),
                token.getUuid(),
                token.getExpDate()));

        return token;
    }


    private String createToken(Map<String, Object> claims, String subject, String id, Date expDate) {
        return Jwts.builder()
                .setSubject(subject)
                .addClaims(claims)
                .setId(id)
                .setIssuedAt(new Date())
                .setExpiration(expDate)
                .signWith(SignatureAlgorithm.HS256, settings.getSecret())
                .compact();
    }

    private Date getExpireDate(int expireMins) {
        return new Date(System.currentTimeMillis() + expireMins * 60 * 1000);
    }
}
