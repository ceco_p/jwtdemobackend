package com.privatephone.adminpanel.security.services;

import com.privatephone.adminpanel.mappers.AdminUsersMapper;
import com.privatephone.adminpanel.models.AdminUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DbUserDetailsService implements UserDetailsService {

    @Autowired
    private AdminUsersMapper adminUsersMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AdminUser adminUser = adminUsersMapper.findAdminUserByName(username);

        if (adminUser == null) {
            throw new UsernameNotFoundException("User " + username + " don not exists.");
        }

        String[] roles = adminUser.getRoles().stream().map(r -> r.toString()).toArray(String[]::new);
        return User.withUsername(adminUser.getUsername())
                .password(adminUser.getPassword())
                .roles(roles)
                .disabled(!adminUser.isActive())
                .build();
    }
}
