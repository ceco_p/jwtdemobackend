package com.privatephone.adminpanel.security;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ApiErrorCode {
    GLOBAL(1), TOKEN_EXPIRED(10), AUTHENTICATION(20);

    private int errorCode;

    ApiErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @JsonValue
    public int getErrorCode() {
        return errorCode;
    }
}
