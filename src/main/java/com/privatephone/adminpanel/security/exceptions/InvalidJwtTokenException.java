package com.privatephone.adminpanel.security.exceptions;

public class InvalidJwtTokenException extends Exception {
    public InvalidJwtTokenException() {

    }

    public InvalidJwtTokenException(String message) {
        super(message);
    }
}
