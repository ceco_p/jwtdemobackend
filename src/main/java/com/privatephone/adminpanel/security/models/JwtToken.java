package com.privatephone.adminpanel.security.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

public class JwtToken {
    private String token;
    private Date expDate;
    private String uuid;
    @JsonIgnore
    private String userName;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
