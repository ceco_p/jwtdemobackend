package com.privatephone.adminpanel.security.models;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class UserContext {
    private String username;
    private Collection<? extends GrantedAuthority> grantedAuthority;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<? extends GrantedAuthority> getGrantedAuthority() {
        return grantedAuthority;
    }

    public void setGrantedAuthority(Collection<? extends GrantedAuthority> grantedAuthority) {
        this.grantedAuthority = grantedAuthority;
    }
}
