package com.privatephone.adminpanel.security.models;

public class JwtAuthenticationResponse {
    private String accessToken;
    private String refreshToken;
    private Long expDate;

    public JwtAuthenticationResponse() {

    }

    public JwtAuthenticationResponse(String accessToken, String refreshToken, Long expDate) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expDate = expDate;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpDate() {
        return expDate;
    }

    public void setExpDate(Long expDate) {
        this.expDate = expDate;
    }
}
