package com.privatephone.adminpanel.security.controllers;

import com.privatephone.adminpanel.security.ApiError;
import com.privatephone.adminpanel.security.ApiErrorCode;
import com.privatephone.adminpanel.security.exceptions.InvalidJwtTokenException;
import com.privatephone.adminpanel.security.models.AuthenticationRequest;
import com.privatephone.adminpanel.security.models.JwtAuthenticationResponse;
import com.privatephone.adminpanel.security.models.JwtToken;
import com.privatephone.adminpanel.security.models.RefreshTokenRequest;
import com.privatephone.adminpanel.security.models.UserContext;
import com.privatephone.adminpanel.security.services.JwtService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.ExpiredJwtException;

@RestController()
public class AuthenticationController {
    private static Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    UserDetailsService userDetailsService;

    @RequestMapping(value = "/api/auth/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createToken(@RequestBody AuthenticationRequest authRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authRequest.getUserName(),
                        authRequest.getPassword()));

        UserContext userInfo = new UserContext();
        userInfo.setUsername(authRequest.getUserName());
        userInfo.setGrantedAuthority(authentication.getAuthorities());
        JwtToken accessToken = jwtService.generateAccessToken(userInfo);
        JwtToken refreshToken = jwtService.generateRefreshToken(userInfo);

        jwtService.saveRefreshToken(refreshToken, authRequest.getUserName());

        return ResponseEntity.ok(new JwtAuthenticationResponse(
                accessToken.getToken(),
                refreshToken.getToken(),
                accessToken.getExpDate().getTime()));
    }

    @RequestMapping(value = "/api/auth/refresh-token", method = RequestMethod.POST)
    public ResponseEntity<?> refreshToken(@RequestBody RefreshTokenRequest token)
            throws InvalidJwtTokenException {

        JwtToken refreshToken = jwtService.parseRefreshToken(token.getRefreshToken());
        jwtService.validateRefreshToken(refreshToken);
        UserDetails userDetails = userDetailsService.loadUserByUsername(refreshToken.getUserName());

        UserContext userInfo = new UserContext();
        userInfo.setUsername(refreshToken.getUserName());
        userInfo.setGrantedAuthority(userDetails.getAuthorities());
        JwtToken accessToken = jwtService.generateAccessToken(userInfo);

        return ResponseEntity.ok(new JwtAuthenticationResponse(
                accessToken.getToken(),
                null,
                accessToken.getExpDate().getTime()));
    }

    @RequestMapping(value = "/api/auth/revoke-token", method = RequestMethod.POST)
    public ResponseEntity<?> revokeToken(@RequestBody RefreshTokenRequest token) throws InvalidJwtTokenException {
        jwtService.revokeToken(token.getRefreshToken());
        return ResponseEntity.ok("");
    }

    @RequestMapping(value = "/api/home", method = RequestMethod.GET)
    public ResponseEntity<?> home() throws InvalidJwtTokenException {

        return ResponseEntity.ok("Home sweet home!");
    }

    @ExceptionHandler({ExpiredJwtException.class, InvalidJwtTokenException.class, UsernameNotFoundException.class})
    private ResponseEntity<?> jwtExpiredHandler(Exception exception) {
        ApiError apiError = new ApiError(HttpStatus.UNAUTHORIZED, exception.getMessage(), ApiErrorCode.AUTHENTICATION);
        return new ResponseEntity(apiError, HttpStatus.UNAUTHORIZED);
    }
}
