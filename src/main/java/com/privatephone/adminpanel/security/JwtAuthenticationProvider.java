package com.privatephone.adminpanel.security;

import com.privatephone.adminpanel.security.auth.JwtAuthenticationToken;
import com.privatephone.adminpanel.security.models.UserContext;
import com.privatephone.adminpanel.security.services.JwtService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class JwtAuthenticationProvider implements AuthenticationProvider {

    private JwtService jwtService;

    public JwtAuthenticationProvider(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String jwtToken = String.valueOf(authentication.getCredentials());
        UserContext userContext = jwtService.parseAccessToken(jwtToken);
        JwtAuthenticationToken jwtAuthentication = new JwtAuthenticationToken(jwtToken, userContext);
        return jwtAuthentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
