package com.privatephone.adminpanel.exceptions;

public class InvalidUserException extends Exception {
    public InvalidUserException() {
        super();
    }

    public InvalidUserException(String message) {
        super(message);
    }

}
