package com.privatephone.adminpanel.mappers;

import com.privatephone.adminpanel.models.AdminRoles;
import com.privatephone.adminpanel.models.AdminUser;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AdminUsersMapper {

    @Select("SELECT * FROM admin_users WHERE username = #{username}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "roles", column = "id", javaType = List.class, many = @Many(select = "selectRoles"))
    })
    AdminUser findAdminUserByName(@Param("username") String username);

    @Select("SELECT ar.role_name FROM admin_roles ar, admin_users_roles aur WHERE ar.id = aur.roles_id  AND aur.admin_user_id = #{user_id}")
    List<AdminRoles> selectRoles(@Param("user_id") long id);
}