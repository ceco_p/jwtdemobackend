package com.privatephone.adminpanel.mappers;

import com.privatephone.adminpanel.models.JwtRefreshTokenInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface JwtTokenMapper {

    @Insert("INSERT INTO jwt_refresh_tokens (uuid, exp_date, user_name) values(#{uuid}, #{expDate}, #{userName})")
    void insetJwtToken(JwtRefreshTokenInfo tokenInfo);

    @Select("SELECT * FROM jwt_refresh_tokens WHERE user_name = #{userName}")
    @Results(value = {
            @Result(property = "uuid", column = "uuid"),
            @Result(property = "expDate", column = "exp_date"),
            @Result(property = "userName", column = "user_name")
    })
    JwtRefreshTokenInfo findByUserName(String userName);

    @Select("SELECT count(uuid) FROM jwt_refresh_tokens WHERE uuid = #{uuid}")
    int countByUuid(String uuid);

    @Update("UPDATE jwt_refresh_tokens SET uuid = #{uuid}, exp_date = #{expDate} WHERE user_name = #{userName}")
    void updateJwtToken(JwtRefreshTokenInfo tokenInfo);

    @Delete("DELETE FROM jwt_refresh_tokens WHERE uuid = #{uuid}")
    void deleteJwtToken(String uuid);
}
