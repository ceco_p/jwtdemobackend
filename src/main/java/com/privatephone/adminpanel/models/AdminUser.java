package com.privatephone.adminpanel.models;

import java.util.List;

public class AdminUser {
    private long id;
    private String username;
    private String password;
    private boolean active;
    private List<AdminRoles> roles;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<AdminRoles> getRoles() {
        return roles;
    }

    public void setRoles(List<AdminRoles> roles) {
        this.roles = roles;
    }
}
