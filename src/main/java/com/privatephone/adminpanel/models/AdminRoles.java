package com.privatephone.adminpanel.models;

public enum AdminRoles {
    ADMIN_USERS, PHONE_USERS, MONITOR_SERVER
}
